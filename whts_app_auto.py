

# Run Part 1 first to save cookies
# Update saved cookies path in Part 2
# Run Part 2

# Part 1

# TO SAVE WEB COOKIES STARTING FIRST TIME

from selenium import webdriver

def save_cookies(driver, filename):
    cookies = driver.get_cookies()
    with open(filename, "w") as file:
        for cookie in cookies:
            file.write(f"{cookie['name']}={cookie['value']}\n")

options = webdriver.ChromeOptions()
# change C:\\ Path to save cookies
options.add_argument("user-data-dir=C:\\")
driver = webdriver.Chrome(options=options)
driver.get("https://web.whatsapp.com/")

input("Scan the QR code and press Enter after logging in to WhatsApp Web.")

save_cookies(driver, "whatsapp_cookies.txt")

driver.quit()

# Part 2

# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# import time

# def load_cookies(driver, filename):
#     with open(filename, "r") as file:
#         cookies = [line.strip().split("=", 1) for line in file]
#         for name, value in cookies:
#             driver.add_cookie({"name": name, "value": value})


# options = webdriver.ChromeOptions()
# # Load the saved cookies path below
# options.add_argument("user-data-dir=C:\\")
# driver = webdriver.Chrome(options=options)
# driver.get("https://web.whatsapp.com/")
# wait=WebDriverWait(driver,100)

# # load_cookies(driver, "whatsapp_cookies.txt")

# target = "target"  # Change this to the name of the contact you want to message
# message = "your message here" #Type the message you want to send here
# number_of_times = 5  # No. of times to send a message
 

# x_arg = f"//span[contains(@title, '{target}')]"
# inp_xpath ='/html[1]/body[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/p[1]'
# group_title = wait.until(EC.presence_of_element_located((By.XPATH, x_arg)))

# group_title.click()
# inp_xpath = '//body/div[@id="app"]/div[1]/div[1]/div[5]/div[1]/footer[1]/div[1]/div[1]/span[2]/div[1]/div[2]/div[1]/div[1]/div[1]/p[1]'
# input_box = wait.until(EC.presence_of_element_located((By.XPATH, inp_xpath)))

# for i in range(number_of_times):
#     input_box.send_keys(message + Keys.ENTER)
#     time.sleep(2)
#     group_title = wait.until(EC.presence_of_element_located((By.XPATH, x_arg)))
#     group_title.click()
#     input_box = wait.until(EC.presence_of_element_located((By.XPATH, inp_xpath)))

# driver.quit()
